import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.XmlListModel 2.15
import QtQuick.Controls 2.15
import Qt.labs.platform 1.1

import FileIO 1.0

Window {
    id: mainWindow
    width: 640
    height: 480
    visible: true
    title: qsTr("Airport AIXM extraction")

    property string xmlSourceUrl

    property string aixmNamespaceDeclarations: "declare namespace faa = 'http://www.faa.gov/aixm5.1';
                                    declare namespace gml = 'http://www.opengis.net/gml/3.2';
                                    declare namespace apt = 'http://www.faa.gov/aixm5.1/apt';
                                    declare namespace xsi = 'http://www.w3.org/2001/XMLSchema-instance';
                                    declare namespace gmd = 'http://www.isotc211.org/2005/gmd';
                                    declare namespace gco = 'http://www.isotc211.org/2005/gco';
                                    declare namespace xlink = 'http://www.w3.org/1999/xlink';
                                    declare namespace aixm = 'http://www.aixm.aero/schema/5.1';"

    property int fsmState: fsm_Ready

    property int fsm_Done: -2
    property int fsm_Ready: -1
    property int fsm_FindAirports: 0
    property int fsm_FindFrequencies: fsm_FindAirports + 1
    property int fsm_FindRunways: fsm_FindFrequencies + 1


    XmlListModel {
        id: aixmAirportModel

        source: (fsmState  === fsm_FindAirports) ? xmlSourceUrl : ""


        // This has to be declared in order to correctly parse the AIXM file
        namespaceDeclarations: aixmNamespaceDeclarations

        // Root for the query
        query: "/faa:SubscriberFile/faa:Member/aixm:AirportHeliport/aixm:timeSlice/aixm:AirportHeliportTimeSlice"

        // query the airport's airport ID
        XmlRole { name: "airport_id"; query: "aixm:designator/string()" }

        // query the airport's AIXM number
        XmlRole { name: "aixm_number"; query: "@gml:id/string()" }

        // query the airport's lat/lon
        XmlRole { name: "lon_lat"; query: "aixm:ARP/aixm:ElevatedPoint/gml:pos/string()" }

        // query the airport's altitude
        XmlRole { name: "altitude"; query: "aixm:fieldElevation/number()" }

        // query the airport's parent sectional
        XmlRole { name: "sectional"; query: "aixm:extension/apt:AirportHeliportExtension/apt:aeronauticalSectionalChart/string()" }

        // query the airport's UNICOM frequency
        XmlRole { name: "unicom"; query: "aixm:extension/apt:AirportHeliportExtension/apt:unicomFrequencies/number()" }

        onStatusChanged: {
            switch (status) {
            case XmlListModel.Null:
                console.log("[XML][aixmAirportModel]: Null")
                break;
            case XmlListModel.Ready:
                console.log("[XML][aixmAirportModel]: Ready, " + count + " elements")

                progressText.text = "Processing " + inputFileDialog.file + "\n\n" + "This may take a while."

                for (let i=0; i<count; i++) {
                    let tmpLonLat = aixmAirportModel.get(i).lon_lat.split(" ")
                    let lon = parseFloat(tmpLonLat[0])
                    let lat = parseFloat(tmpLonLat[1])

                    let tmpVar = aixmAirportModel.get(i).aixm_number.split("_")
                    let aixm_number = tmpVar[tmpVar.length-1]

                    frequencyListModel.append(
                                {
                                    airport_id: aixmAirportModel.get(i).airport_id,
                                    aixm_number: aixm_number,
                                    lat_D: lat,
                                    lon_D: lon,
                                    altitude: aixmAirportModel.get(i).altitude,
                                    sectional: aixmAirportModel.get(i).sectional,
                                    unicom: aixmAirportModel.get(i).unicom ? aixmAirportModel.get(i).unicom : -1,
                                    frequency: -1
                                })
                }

                fsmState = fsm_FindFrequencies

                break;
            case XmlListModel.Loading:
                console.log("[XML][aixmAirportModel]: Loading")
                break;
            case XmlListModel.Error:
                console.log("[XML][aixmAirportModel]: Error: " + errorString())
                progressText.text = "Error loading " + inputFileDialog.file + "\n" + "\t" + errorString()
                break;
            }

        }
    }

    XmlListModel {
        id: aixmAirportFrequencyModel

        source: (fsmState === fsm_FindFrequencies) ? xmlSourceUrl : ""


        // This has to be declared in order to correctly parse the AIXM file
        namespaceDeclarations: aixmNamespaceDeclarations

        // Root for the query
        query: "/faa:SubscriberFile/faa:Member/aixm:RadioCommunicationChannel/aixm:timeSlice/aixm:RadioCommunicationChannelTimeSlice"

        // query the airport's AIXM number
        XmlRole { name: "aixm_number"; query: "@gml:id/string()" }

        // query the airport's radio frequency
        XmlRole { name: "frequency"; query: "aixm:frequencyTransmission/number()" }

        onStatusChanged: {
            switch (status) {
            case XmlListModel.Null:
                console.log("[XML][aixmAirportFrequencyModel]: Null")
                break;
            case XmlListModel.Ready:
                console.log("[XML][aixmAirportFrequencyModel]: Ready, " + count + " elements")

                for (let i=0; i<count; i++) {
                    let tmpVar = aixmAirportFrequencyModel.get(i).aixm_number.split("_")
                    let aixm_number = tmpVar[tmpVar.length-1]

                    let frequency = aixmAirportFrequencyModel.get(i).frequency

                    // Only find the airport if there is a frequency to be added
                    if (frequency.length !== 0) {
                        // Find the airport item in the ListModel
                        let airportItem = frequencyListModel.find(function(item) { return item.aixm_number === aixm_number})

                        airportItem.frequency = frequency
                    }
                }

                fsmState = fsm_Done

                break;
            case XmlListModel.Loading:
                console.log("[XML][aixmAirportFrequencyModel]: Loading")
                break;
            case XmlListModel.Error:
                console.log("[XML][aixmAirportFrequencyModel]: Error: " + errorString())
                break;
            }

        }
    }

    ListModel {
        id: frequencyListModel

        function find(criteria) {
          for(var i = 0; i < count; ++i) {
              if (criteria(get(i))) {

                  return get(i)}
          }
          return null
        }
    }

    ListModel {
        id: publishListModel
    }

    ListView {
        model: publishListModel
        anchors.fill: parent
        delegate: Text {
            text: "airport: " + airport_id + "  \tunicom: " + unicom + "     \tfreq: " + frequency
        }
    }

    TextEdit {
        id: progressText
        anchors.centerIn: parent
        text: "To download the AIXM file go to: \nhttps://www.faa.gov/air_traffic/flight_info/aeronav/aero_data/\n\nFollow the links: \n--> 28 Day NASR Subscription --> Current --> Download.\n\n Click `Load AIXM` in the top right corner and point\n to the file you just downloaded and unzipped."

        readOnly: true
        selectByMouse: true
    }

    Button {
        id: saveButton
        text: "Save output"

        enabled: publishListModel.count > 0

        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: 10

        onClicked: {
            ouputFileDialog.open()
        }
    }
    Button {
        id: loadButton
        text: "Load AIXM"

        anchors.top: parent.top
        anchors.right: saveButton.left
        anchors.margins: 10

        onClicked: {
            inputFileDialog.open()
        }
    }


    FileDialog {
        id: inputFileDialog

        fileMode: FileDialog.OpenFile
        nameFilters: ["XML files (*.xml)"]
        options: FileDialog.ReadOnly

        onFolderChanged: {
            console.log("folder: " + folder)

        }

        onAccepted: {
            xmlSourceUrl = file

            fsmState = fsm_FindAirports
            loadButton.enabled = false

            progressText.text = "Loading " + inputFileDialog.file + "\n\n" + "This may take a while."
        }
    }

    FileDialog {
        id: ouputFileDialog

        defaultSuffix:'json'
        fileMode: FileDialog.SaveFile

        onAccepted: {
            fileIO_Output.filename = file

            var datamodel = []
            for (let i = 0; i < publishListModel.count; i++) {
                datamodel.push(publishListModel.get(i))
            }

            let outputText = JSON.stringify(datamodel)

            fileIO_Output.writeString(outputText)
            fileIO_Output.close()
        }
    }

    FileIO {
        id: fileIO_Output
        filename: "file:///Users/kenz/Downloads/AIXM_5.1/XML-Subscriber-Files/bob.json"
    }

    onFsmStateChanged: {
        if (fsmState === fsm_Done) {
            pruneData()

            loadButton.enabled = true
            progressText.text = ""

        }
    }

    function pruneData() {
        publishListModel.clear()

        for (let i = 0; i < frequencyListModel.count; i++) {
            publishListModel.append({
                                        airport_id: frequencyListModel.get(i).airport_id,
                                        lat_D: frequencyListModel.get(i).lat_D,
                                        lon_D: frequencyListModel.get(i).lon_D,
                                        altitude: frequencyListModel.get(i).altitude,
                                        sectional: frequencyListModel.get(i).sectional,
                                        unicom: frequencyListModel.get(i).unicom,
                                        frequency: frequencyListModel.get(i).frequency
                                    })

        }
    }
}
