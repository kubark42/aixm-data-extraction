# AIXM Data Extraction

This utility extracts data from AIXM files.

## Releases
**Releases**: https://gitlab.com/kubark42/aixm-data-extraction/-/releases

## Instructions

Follow the instructions in the UI window.

This requires the up-to-date AIXM source file. To download it, go to: https://www.faa.gov/air_traffic/flight_info/aeronav/aero_data/. Follow the links: `28 Day NASR Subscription` --> `Current` --> `Download`. Click `Load AIXM` in the top right corner and point to the file you just downloaded and unzipped.

## Screenshots

![](https://gitlab.com/kubark42/aixm-data-extraction/uploads/d4a99fda98b66f1b060309aefb82078b/Screen_Shot_2021-09-14_at_19.55.03.png)

## Building
Almost bog standard Qt build.

1. Load `AixmDataExtraction.pro` into Qt Creator and hit the big build button. This should always work, if the build fails, let me know and I'll fix it.
